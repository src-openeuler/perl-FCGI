# Perform functional tests using FCGI::Client.
# RHEL does not include FCGI::Client due to its dependencies.
%bcond_with perl_FCGI_enables_client_tests

Name:           perl-FCGI
Epoch:          1
Version:        0.82
Release:        3
Summary:        FastCGI Perl bindings
License:        OML
URL:            https://metacpan.org/release/FCGI
Source0:        https://cpan.metacpan.org/authors/id/E/ET/ETHER/FCGI-%{version}.tar.gz
Patch0:		    FCGI-0.78-CVE-2012-6687.patch

# bash for sh executed from Makefile.PL
BuildRequires:  bash
BuildRequires:  coreutils
BuildRequires:  findutils
BuildRequires:  gcc
# grep executed by configure
BuildRequires:  grep
BuildRequires:  make
BuildRequires:  perl-devel
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(:VERSION) >= 5.6
BuildRequires:  perl(Config)
BuildRequires:  perl(Cwd)
# ExtUtils::Liblist not used
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(File::Copy)
# File::Spec not used on Linux
BuildRequires:  perl(Getopt::Long)
BuildRequires:  perl(IO::File)
# sed executed by configure
BuildRequires:  sed
# Run-time:
# Carp not used at tests
BuildRequires:  perl(strict)
BuildRequires:  perl(XSLoader)
# Tests:
BuildRequires:  perl(Test)
%if %{with perl_FCGI_enables_client_tests}
BuildRequires:  perl(FCGI::Client)
BuildRequires:  perl(File::Temp)
BuildRequires:  perl(IO::Socket)
BuildRequires:  perl(Test::More) >= 0.88
BuildRequires:  perl(warnings)
%endif
Requires:       perl(Carp)
Requires:       perl(XSLoader)
# fcgiapp.c, os_unix.c, os_win32.c are copied and modified from FastCGI
# Developer's Kit of an unknown version, bug #736612
Provides:       bundled(fcgi)

%{?perl_default_filter}

%description
FastCGI Perl bingdings.

%package       help
Summary:       Documentation for perl-FCGI
Requires:      perl-FCGI = %{epoch}:%{version}-%{release}
Provides:      perl-FCGI-doc = %{epoch}:%{version}-%{release}
Obsoletes:     perl-FCGI-doc < %{epoch}:%{version}-%{release}

%description   help
Documentation for developing programs that will use FCGI.

%prep
%autosetup -n FCGI-%{version} -p1
find . -type f -exec chmod -c -x {} +
%if %{without perl_FCGI_enables_client_tests}
rm -f t/02-unix_domain_socket.t
perl -i -ne 'print $_ unless m{^t/02-unix_domain_socket\.t}' MANIFEST
%endif

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}" NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%{perl_vendorarch}/*
%exclude %dir %{perl_vendorarch}/auto

%files help
%{_mandir}/man3/*.3*
%doc ChangeLog README

%changelog
* Fri Jan 10 2025 pengjian <pengjian23@mails.ucas.ac.cn> - 1:0.82-3
- Introduced environment variable-controlled polling timeouts 
for the close and connection-keeping functions in the os_unix.c file.

* Thu Aug 17 2023 yang-chaohao <yangchaohao@huawei.com> - 1:0.82-2
- The bcond definition statement format is not recognized. Modified its definition format

* Wed Jul 19 2023 leeffo <liweiganga@uniontech.com> - 1:0.82-1
- upgrade to version 0.82

* Wed Jun 09 2021 zhaoyao<zhaoyao32@huawei.com> - 1:0.78-12
- %prep no longer patched with git

* Tue Apr 14 2020 Jeffery.Gao <gaojianxing@huawei.com> - 1:0.78-11
- Package init